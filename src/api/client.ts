import express from 'express'
import {getClients, sendAdminCommand, sendBroadcastMessage, sendPrivateMessage} from '../services/openttd'
import {respondOk} from './common'

const router = express.Router()

router.get('/', (req, res) => {
  return getClients().then(res.json.bind(res))
})

router.post('/:clientId/message', (req,res) => {
  const message = req.body.message
  if (!message) {
    res.status(400).send('Body must contain a message')
    return
  }

  const clientId = parseInt(req.params.clientId)
  
  console.log(`API: ${req.params.clientId} : ${message}`)
  sendPrivateMessage(clientId, message)
    
  respondOk(res)('')
})

router.delete('/:clientId', (req, res) => {
  const command = 'ban' in req.query ? 'ban' : 'kick'
  const queryReason = 'reason' in req.query ? req.query.reason?.toString() : ''
  const reason = 'reason' in req.query ? `"${queryReason.replace('"', '\"')}"` : 'Bye'
  
  return sendAdminCommand(`${command} ${req.params.clientId} ${reason}`)
    .then(respondOk(res))
})

router.post('/message', (req, res) => {
  const message = req.body.message
  if (!message) {
    res.status(400).send('Body must contain a message')
    return
  }
  
  sendBroadcastMessage(message)
    
  respondOk(res)('')
})
export default router