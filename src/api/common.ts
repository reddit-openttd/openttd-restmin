export const respondOk = (res) => (rconResponse: string) => {
  if (rconResponse.startsWith('ERROR')) {
    return res.status(400).send(rconResponse)
  }
  return res.json({message:rconResponse})
}