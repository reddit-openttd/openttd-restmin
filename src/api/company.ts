import express from 'express'
import {sendAdminCommand}  from '../services/openttd'

const router = express.Router()


router.get('/', (req, res) => {
  return sendAdminCommand('companies')
    .then(companiesResponse => {
      const companies = companiesResponse
        .split('\n')
        .filter(maybeEmptyString => maybeEmptyString)
      const companyArr = companies.map(company => {
        // #:2(Pink) Company Name: 'ninjasecreto Transport'  Year Founded: 1940  Money: -43041  Loan: 150000  Value: 1  (T:0, R:8, P:1, S:0) unprotected
        // #:1(Pink) Company Name: 'efess Transport'  Year Founded: 1941  Money: 99425  Loan: 100000  Value: 1  (T:0, R:0, P:0, S:0) unprotected
        const groups = /#:([0-9]+)\((.+?)\)\s+Company Name: '(.*)'\s+Year Founded: ([0-9]+)\s+Money:\s+(-?[0-9]+)\s+Loan:\s+(-?[0-9]+)\s+Value:\s+(-?[0-9]+)\s+\(T:([0-9]+), R:([0-9]+), P:([0-9]+), S:([0-9]+)\)\s+(.+)/
          .exec(company)
        try {
          return {
            id: parseInt(groups[1]),
            color: groups[2],
            name: groups[3],
            incorporated: groups[4],
            balance: parseInt(groups[5]),
            loan: parseInt(groups[6]),
            value: parseInt(groups[7]),
            trains: parseInt(groups[8]),
            roadVehicles: parseInt(groups[9]),
            planes: parseInt(groups[10]),
            ships: parseInt(groups[11]),
            password: groups[12] === 'protected'
          }
        } catch(err: any) {
          console.log(`Error parsing company string (${err.message}):`)
          console.log(company)
        }
      })
      return res.json(companyArr)
    })
})

router.delete('/:companyId', (req, res) => {
  return sendAdminCommand(`reset_company ${req.params.companyId}`)
    .then(companiesResponse => {
      if (companiesResponse.startsWith('ERROR')) {
        return res.status(400).send(companiesResponse)
      }
      return res.json({message:companiesResponse})
    })
})
export default router