import express from 'express'
import client from './client'
import company from './company'
import game from './game'
import rcon from './rcon'

import {errorHandler} from './middleware/error'

const router = express.Router()

router.use('/game', game)
router.use('/client', client)
router.use('/company', company)
router.use('/rcon', rcon)
router.use(errorHandler)

export default router