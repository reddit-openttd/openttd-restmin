import express from 'express'
import {sendAdminCommand}  from '../services/openttd'
import { InterfaceError } from './error'

const router = express.Router()

router.post('/', (req, res) => {
  const command = req.body.command
  if (!command) {
    return res.status(400).send('No command specified.')
  }
  return sendAdminCommand(command)
    .then(response => {
      res.send(response)
    })
    .catch(err => new InterfaceError(err.message))
})

export default router