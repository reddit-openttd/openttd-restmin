import {onJoin, onQuit, onChat, onPrivateChat, onNewGame, onWelcome, onShutDown, onConsole, onDate} from '../services/openttd'
import {publishOpenTTDEvent} from './publish'
import { GameInfo } from '../types/GameInfo'
import {Chat, ConsoleEvent} from '../types/OpenTTDEvent'

export const init = () => {
  onDate((date: number) => publishOpenTTDEvent('date', date))
  onConsole((event: ConsoleEvent) => publishOpenTTDEvent('console', event))
  onShutDown(() => publishOpenTTDEvent('shutdown'))
  onWelcome((gameInfo: GameInfo) => publishOpenTTDEvent('welcome', gameInfo))
  onNewGame(() => publishOpenTTDEvent('newgame'))
  onJoin((clientId: number) => publishOpenTTDEvent('clientjoin', {clientId}))
  onQuit((clientId: number) => publishOpenTTDEvent('clientquit', {clientId}))
  onChat((chatDetail: Chat) => publishOpenTTDEvent('chat', chatDetail))
  onPrivateChat((chatDetail: Chat) => publishOpenTTDEvent('chat-private', chatDetail))
}