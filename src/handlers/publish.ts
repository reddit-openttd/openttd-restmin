import {publish} from '../services/rabbitmq'
import {OpenTTDEventType} from '../types/OpenTTDEvent'

export const publishOpenTTDEvent =  (event: OpenTTDEventType, data?: any) => publish({
  event,
  serverName: process.env.OPENTTD_NAME! || '',
  serverHost: {
    url: process.env.API_HTTP_URL + ":" + process.env.API_HTTP_PORT
  }, 
  data
})
