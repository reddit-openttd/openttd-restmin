import dotenv from 'dotenv'
import express from 'express'
import * as openttd from './services/openttd'
import * as rabbitmq from './services/rabbitmq'
import * as handlers from './handlers'
import api from './api'
import sanityCheck from './sanity'

dotenv.config()

sanityCheck()

rabbitmq.init()
openttd.init()
handlers.init()

const app = express()

if (!process.env.API_HTTP_PORT) {
  process.env.API_HTTP_PORT = '8080'
}
const port = process.env.API_HTTP_PORT

app.use(express.json())
app.use('/api', api)

app.listen(port, () => {
  console.log(`Restmin API listening on port ${port}`)
}) 

