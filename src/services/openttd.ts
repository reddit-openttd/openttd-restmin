import ottd from 'node-openttd-admin'
import events from 'events'
import { Chat, ConsoleEvent } from '../types/OpenTTDEvent'
import {GameInfo} from '../types/GameInfo'
import { Client } from '../types/OpenTTDApi'

const SERVER_ID = 0x01

const enums = ottd.enums

let _gameInfo: GameInfo = null
let connection = null
let rconSemaphore = Promise.resolve('')

export const openttdEmitter = new events.EventEmitter();

const reconnect = () => {
  connection.sock.connect(
    { 
      host:process.env.OPENTTD_HOST, 
      port:parseInt(process.env.OPENTTD_PORT) 
    }
  )
}
const start = () => {
  connection = new ottd.connection();
  connection.on('error', (err) => {
    if (err === 'connectionclose') {
      console.log(new Date().toLocaleTimeString() + ' Connection was closed. Restarting in 1 seconds.')
      setTimeout(() => reconnect(), 1000)
    }
  })
  connection.on('connect', function(){
    console.log('Connected to server.')
    connection.authenticate(process.env.OPENTTD_USER, process.env.OPENTTD_PASSWORD);
  });

  connection.on('welcome', async (gameInfo: GameInfo) => {
    _gameInfo = gameInfo
    connection.send_update_frequency(enums.UpdateTypes.DATE, enums.UpdateFrequencies.DAILY);
    connection.send_update_frequency(enums.UpdateTypes.CLIENT_INFO, enums.UpdateFrequencies.AUTOMATIC);
    connection.send_update_frequency(enums.UpdateTypes.CHAT, enums.UpdateFrequencies.AUTOMATIC);
    connection.send_update_frequency(enums.UpdateTypes.CONSOLE, enums.UpdateFrequencies.AUTOMATIC);
    
    openttdEmitter.emit('welcome', _gameInfo)
  });

  connection.on('newgame', async (r) => {
    openttdEmitter.emit('newgame')
  })

  connection.on('shutdown', async () => {
    openttdEmitter.emit('shutdown')
  })
  connection.on('console', async (consoleEvent: {output: string, origin: string}) => {
    openttdEmitter.emit('console', consoleEvent)
  })
  connection.on('clientjoin', async (clientId: number) => {
    openttdEmitter.emit('clientjoin', clientId)
  })
  connection.on('clientquit', function(id){
    openttdEmitter.emit('clientquit', id)
  });
  connection.on('chat', function({action, desttype , id, message, money} ){
     if (action === enums.Actions.CHAT) {
      openttdEmitter.emit('chat', {clientId: id, message, money})
     } else if (action === enums.Actions.CHAT_CLIENT) {
      openttdEmitter.emit('chat-private', {clientId: id, message, money})
     }
  });

  connection.on('date', function(date){
    _gameInfo.date = date
    openttdEmitter.emit('date', date)
  });

  connection.connect(process.env.OPENTTD_HOST, parseInt(process.env.OPENTTD_PORT) || 3977);
}

export const init = () => {
  start()
  return openttdEmitter
}

export const onNewGame = (handler: () => void) => {
  openttdEmitter.on('newgame', handler)
}

export const onWelcome = (handler: (gameInfo: GameInfo) => void) => {
  openttdEmitter.on('welcome', handler)
}

export const onShutDown = (handler: () => void) => {
  openttdEmitter.on('shutdown', handler)
}

export const onDate = (handler: (date: number) => void) => {
  openttdEmitter.on('date', handler)
}

export const onJoin = (handler: (clientId: number) => void) => {
  openttdEmitter.on('clientjoin', handler)
}

export const onQuit = (handler: (clientId: number) => void) => {
  openttdEmitter.on('clientquit', handler)
}

export const onChat = (handler: (chatDetail: Chat) => void) => {
  openttdEmitter.on('chat', handler)
}

export const onPrivateChat = (handler: (chatDetail: Chat) => void) => {
  openttdEmitter.on('chat-private', handler)
}

export const onConsole = (handler: (event: ConsoleEvent) => void) => {
  openttdEmitter.on('console', handler)
}

export const getGameInfo = (): GameInfo => {
  return _gameInfo
}

export const sendPrivateMessage = (clientId: number, message: string) => {
  connection.send_chat(enums.Actions.CHAT_CLIENT, enums.DestTypes.CLIENT, clientId, message)
}

export const sendBroadcastMessage = (message: string) => {
  connection.send_chat(enums.Actions.CHAT, enums.DestTypes.BROADCAST, SERVER_ID, message)
}

export const sendAdminCommand = async (command: string): Promise<string> => {
  rconSemaphore = rconSemaphore.then(() => {
    return new Promise((resolve, reject) => {
      const timeoutId = setTimeout(() => reject('timed out waiting for rconend'), 5000)
      const outputs = []

      const rconOutput = ({colour, output}) => {
        outputs.push(output)
      }
      const rconEnd = () => {
        clearTimeout(timeoutId)
        removeHandlers()
        resolve(outputs.join('\n'))
      }
      const rconError = (err) => {
        clearTimeout(timeoutId)
        removeHandlers()
        reject('Somethings not working: ' + err)
      }
      const removeHandlers = () => {
        connection.removeListener('rcon', rconOutput)
        connection.removeListener('rconend', rconEnd)
        connection.removeListener('error', rconError)
      }
      connection.on('rcon', rconOutput)
      connection.on('rconend', rconEnd)
      connection.on('error', rconError)
  
      connection.send_rcon(command)
    })
  })

  return rconSemaphore
}

export const getClients = ():Promise<Client[]> => {
  return sendAdminCommand('clients')
    .then(clientResponse => {
      const clients = clientResponse
        .split('\n')
        .filter(maybeEmptyString => maybeEmptyString)

      return clients.map(client => {        
        const groups = /^Client #(\d+)\s+name:\s+'(.*)'\s+company:\s+(\d+)\s+IP:\s+(.*)$/.exec(client.trim())
        if (!groups || groups.length !== 5) {
          console.log('Could not parse client with string:')
          console.log(client)
          return null
        }
        return {
          id: parseInt(groups[1]),
          name: groups[2],
          companyId: parseInt(groups[3]),
          ip: groups[4]
        }
      })
      .filter((client: Client | null) => client !== null) as Client[]
    })
}