import amqp from 'amqplib'
import { OpenTTDEvent } from '../types/OpenTTDEvent';

// if the connection is closed or fails to be established at all, we will reconnect
let amqpConn = null;
const EVENTS_EXCHANGE = 'openttd-events'

const start = async () => {
  try {
    const host = process.env.RABBIT_MQ_HOST + (process.env.RABBIT_MQ_PORT ? `:${process.env.RABBIT_MQ_PORT}` : '')
    amqpConn = await amqp.connect('amqp://' + host + "?heartbeat=60")
  }
  catch(err) {
    console.error("[AMQP]", err.message);
    return new Promise((resolve) => setTimeout(() => resolve(start()), 1000))
  }

  amqpConn.on("error", function(err) {
    if (err.message !== "Connection closing") {
      console.error("[AMQP] conn error", err.message);
    }
  })

  amqpConn.on("close", function() {
    console.error("[AMQP] reconnecting");
    return new Promise((resolve) => setTimeout(() => resolve(start()), 1000))
  });
  return onConnected();
}

var pubChannel = null;
var offlinePubQueue = [];

const assertSetup = () => {
  return amqpConn.createChannel()
    .then(channel => channel.assertExchange(EVENTS_EXCHANGE, 'fanout', {durable: false}))
}

const onConnected = () => {
  return assertSetup()
    .then(() => startPublisher())
}

const startPublisher = () => {
  return amqpConn.createConfirmChannel()
    .then(channel => {
      channel.on("error", function(err) {
        console.error("[AMQP] channel error", err.message);
      });
      channel.on("close", function() {
        console.log("[AMQP] channel closed");
      });
      pubChannel = channel;
      while (offlinePubQueue.length) {
        publish(offlinePubQueue.shift());
      }
    })
    .catch(err => {
      console.error("[AMQP] error", err);
      amqpConn.close();
    })
}

export const publish = (content: OpenTTDEvent<unknown>)  => {
  if (!pubChannel) {
    console.error("[AMQP] Is not connected, queueing message");
    offlinePubQueue.push(content);
    return;
  }
  try { 
    pubChannel.publish(EVENTS_EXCHANGE, '', Buffer.from(JSON.stringify(content)), { persistent: true })
  }
  catch(err) {
    console.error("[AMQP] publish", err);
    offlinePubQueue.push(content);
    if (pubChannel && pubChannel.connection) {pubChannel.connection.close()} 
  }
}

export const init = () => {
  start()
}