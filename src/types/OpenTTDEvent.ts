export type OpenTTDEventType = 'newgame' | 'welcome' | 'shutdown' | 'date' | 'clientjoin' | 'clientquit' | 'chat' | 'chat-private' | 'console' | 'system'

export type HttpServerHost = {
  url: string
}

export type ClientEvent = {
  clientId: number
  companyId?: number,
  name?: string,
  ip?: string
}

export type Chat = ClientEvent & {
  message: string,
  money: number
}

export type ConsoleEvent = {
  origin: string,
  output: string
}

export type SystemEvent = {

}

export type OpenTTDEvent<T> = {
  serverHost: HttpServerHost,
  serverName: string,
  event: OpenTTDEventType,
  data: T
}